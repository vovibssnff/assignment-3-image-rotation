#include "../include/core.h"

static void shuffle(struct image* image) {
    uint64_t height = image->width;
    image->width = image->height;
    image->height = height;
}

static int angle_transform(int angle) {
    if (angle < 0) {
        angle += DEGREES_360;
        angle %= DEGREES_360;
        return DEGREES_360-angle;
    }
    return DEGREES_360-angle;
}

static void rotate_by_quarter(struct image* src, struct image* target) {
    for (size_t i=0; i<src->height; i++) {
        for (size_t j=0; j<src->width; j++) {
            size_t rotated_i = j;
            size_t rotated_j = src->height - i - 1;
            target->data[src->height*rotated_i+rotated_j] = src->data[src->width*i+j];
        }
    }
}

enum rotate_status rotate(struct image* src, struct image* target, int angle) {
    if (src->width==0||src->height==0) {
        return ROTATE_ERROR;
    }
    angle = angle_transform(angle);
    size_t n = angle / 90;
    set_image(target, src->width, src->height);
    for (size_t i = 0; i < n; i++) {
        shuffle(target);
        rotate_by_quarter(src, target);
        destroy_image(src);
        *src = copy_image(target);
    }
    return ROTATE_OK;
}

