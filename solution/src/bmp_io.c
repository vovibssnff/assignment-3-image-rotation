#include "../include/bmp_io.h"

inline size_t padding_calculation(const size_t width) {
    size_t bytes = 3*width;
    return (4-bytes%4)%4;
}

enum read_status from_bmp(FILE** file, struct image* image) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, *file) != 1) {
        return READ_INVALID_BITS;
    }
    if (header.biSize != HEADER_SIZE) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != HEADER_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != HEADER_BITS) {
        return READ_INVALID_VOLUME;
    }
    
    fseek(*file, header.bOffBits, SEEK_SET);
    set_image(image, header.biWidth, header.biHeight);

    enum pixel_status pixel_status = read_pixels(file, image);

    if (pixel_status!=PIXEL_OK) {
        return READ_INVALID_PIXEL;
    }
    return READ_OK;
}

static void cast_header(struct bmp_header* header, int64_t width, int64_t heigth) {
    header->bfType = HEADER_TYPE;
    header->bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel)*(width+padding_calculation(width))*heigth;
    header->bfReserved = 0;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = HEADER_SIZE;
    header->biWidth = width;
    header->biHeight = heigth;
    header->biPlanes = 1;
    header->biBitCount = HEADER_BITS;
    header->biCompression = 0;
    header->biSizeImage = 0;
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
}

enum write_status to_bmp(FILE** file, struct image* image) {
    struct bmp_header header = {0};
    cast_header(&header, (int64_t)image->width, (int64_t)image->height);

    if (fwrite(&header, sizeof(struct bmp_header), 1, *file)!=1) {
        return WRITE_ERROR;
    }

    if (fseek(*file, header.bOffBits, SEEK_SET)!=0) {
        return WRITE_ERROR;
    }

    if (write_pixels(file, image)!=PIXEL_OK) {
        return WRITE_ERROR;
    }

    return WRITE_OK;
}
