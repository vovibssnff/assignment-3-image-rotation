#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/image.h"
#include "../include/bmp_io.h"

void set_image(struct image* image, const uint64_t width, const uint64_t heigth) {
    image->data = malloc(width*heigth*sizeof(struct pixel));
    image->width = width;
    image->height = heigth;
}

struct image copy_image(const struct image* src) {
    struct image copy;
    copy.width = src->width;
    copy.height = src->height;
    copy.data = malloc(copy.width * copy.height * sizeof(struct pixel));
    if (copy.data == NULL) {
        exit(EXIT_FAILURE);
    }
    for (size_t i = 0; i < copy.width * copy.height; i++) {
        copy.data[i] = src->data[i];
    }
    return copy;
}


enum pixel_status read_pixels(FILE** file, struct image* image) {
    size_t padding = padding_calculation(image->width);
    for (size_t i=0; i<image->height; i++) {
        size_t read = fread(&image->data[i*image->width], sizeof(struct pixel)*image->width, 1, *file);
        if (read!=1) {
            destroy_image(image);
            return PIXEL_ERROR;
        }
        if (fseek(*file, (long) padding, SEEK_CUR)!=0) {
            destroy_image(image);
            return PIXEL_ERROR;
        }
    }
    return PIXEL_OK;
}

void destroy_image(struct image* image) {
    if (image->data) {
        free(image->data);
        image->data = NULL;
    }
}

enum pixel_status write_pixels(FILE** file, struct image* image) {
    size_t padding = padding_calculation(image->width);
    for (size_t i=0; i<image->height; i++) {
        if (fwrite(&image->data[i*image->width], sizeof(struct pixel)*image->width, 1, *file)!=1) {
            return PIXEL_ERROR;
        }

        char *filler = malloc(padding);
        if (filler == NULL) {
            return PIXEL_ERROR;
        }

        for (size_t j = 0; j < padding; j++) {
            filler[j] = 0;
        }

        if (fwrite(filler, sizeof(uint8_t), padding, *file) < padding) {
            free(filler);
            return PIXEL_ERROR;
        }

        free(filler);
    }
    return PIXEL_OK;
}
