#include <stdio.h>
#include <stdlib.h>

#include "../include/core.h"
#include "../include/bmp_io.h"
#include "../include/file_handler.h"
#include "../include/validator.h"

#define READ_MODE "rb"
#define WRITE_MODE "wb"

int main( int argc, char** argv ) {
    if (argc!=4) {return ARGC_ERROR;}
    int angle = atoi(argv[3]);

    // Первичное чтение из файла
    FILE* file = {0};
    enum file_status f_status = open_file(&file, argv[1], READ_MODE);
    if (f_status!=FILE_OK) {
        perror("Cannot open file");
        return f_status;
    }

    // Преобразование к внутреннему формату
    struct image image;
    enum read_status read_status = from_bmp(&file, &image);
    if (read_status!=READ_OK) {
        return read_status;
    }

    fclose(file);

    // Каст результирующей картинки, инициализация полей структуры
    struct image result;

    // Мэджик
    enum rotate_status r_status = rotate(&image, &result, angle);
    if (r_status!=ROTATE_OK) {
        return r_status;
    }
    destroy_image(&image);

    FILE* out = {0};
    f_status = open_file(&out, argv[2], WRITE_MODE);
    if (f_status!=FILE_OK) {
        perror("Cannot open file");
        return f_status;
    }

    enum write_status write_status = to_bmp(&out, &result);
    if (write_status!=WRITE_OK) {
        perror("Cannot write");
        return write_status;
    }

    fclose(out);
    destroy_image(&result);

    return 0;
}
