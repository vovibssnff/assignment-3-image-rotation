#include <stdio.h>

#include "../include/bmp_io.h"
#include "../include/file_handler.h"


enum file_status open_file(FILE** file, const char* fname, const char* mode) {
    FILE* f = fopen(fname, mode);
    if (!f) {
        return FILE_ERROR;
    }
    *file = f;
    return FILE_OK;
}
