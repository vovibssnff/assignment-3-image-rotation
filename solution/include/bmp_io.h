#include "../include/image.h"

#define FREAD_OK 1
#define HEADER_SIZE 40
#define HEADER_TYPE 0x4D42
#define HEADER_BITS 24

#ifndef BMP_IO
#define BMP_IO

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE = 20,
    READ_INVALID_BITS = 21,
    READ_INVALID_HEADER = 22,
    READ_INVALID_VOLUME = 23,
    READ_INVALID_IMAGE = 24,
    READ_INVALID_PIXEL = 25
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

struct image;

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

size_t padding_calculation(size_t width);
enum read_status from_bmp(FILE** file, struct image* image);
enum write_status to_bmp(FILE** file, struct image* image);

#endif
