#include <stdint.h>
#include <stdio.h>

#define DEGREES_360 360

#ifndef IMAGE
#define IMAGE

enum pixel_status {
    PIXEL_OK = 0,
    PIXEL_ERROR = 40
};

struct pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

void set_image(struct image* image, uint64_t width, uint64_t heigth);

struct image copy_image(const struct image* src);

enum pixel_status read_pixels(FILE** file, struct image* image);
 
void destroy_image(struct image* image);

enum pixel_status write_pixels(FILE** file, struct image* image);

#endif
