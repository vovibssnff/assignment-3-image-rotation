#include <stdio.h>

#ifndef FILE_HANDLER
#define FILE_HANDLER

enum file_status {
    FILE_OK,
    FILE_ERROR
};

enum file_status open_file(FILE** file, const char* name, const char* mode);

#endif
