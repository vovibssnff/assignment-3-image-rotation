#include "./image.h"

#ifndef CORE
#define CORE

enum rotate_status {
    ROTATE_OK = 0,
    ROTATE_ERROR
};

enum rotate_status rotate(struct image* src, struct image* target, int angle);

#endif
